# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Badwolf Authors <https://hacktivis.me/projects/badwolf>
# This file is distributed under the same license as the Badwolf package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Badwolf 1.0.3+gedbbb27.develop\n"
"Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me\n"
"POT-Creation-Date: 2021-04-09 08:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: downloads.c:117
#, c-format
msgid "%02i:%02i:%02i Download cancelled"
msgstr ""

#: downloads.c:119
#, c-format
msgid "%02i:%02i:%02i Download error"
msgstr ""

#: downloads.c:141
#, c-format
msgid "%02i:%02i:%02i Download finished"
msgstr ""

#: downloads.c:167
#, c-format
msgid "%02i:%02i:%02i Downloading…"
msgstr ""

#: downloads.c:199 downloads.c:216 downloads.c:219
msgid "Badwolf Downloads"
msgstr ""

#: bookmarks.c:116
msgid "Bookmarks: Done.\n"
msgstr ""

#: bookmarks.c:59
#, c-format
msgid "Bookmarks: Found %d bookmarks.\n"
msgstr ""

#: bookmarks.c:81
#, c-format
msgid "Bookmarks: No loadable file found at %s\n"
msgstr ""

#: bookmarks.c:85
#, c-format
msgid "Bookmarks: loading at %s\n"
msgstr ""

#: bookmarks.c:99
msgid "Bookmarks: unable to create new XPath context\n"
msgstr ""

#: bookmarks.c:107
#, c-format
msgid "Bookmarks: unable to evaluate XPath expression \"%s\"\n"
msgstr ""

#: bookmarks.c:89
#, c-format
msgid "Bookmarks: unable to parse file \"%s\"\n"
msgstr ""

#: badwolf.c:1034
#, c-format
msgid "Buildtime WebKit version: %d.%d.%d\n"
msgstr ""

#: badwolf.c:440
msgid "Continue"
msgstr ""

#: badwolf.c:390
msgid ""
"Couldn't verify the TLS certificate to ensure a better security of the "
"connection. You might want to verify your machine and network.\n"
"\n"
msgstr ""

#: badwolf.c:100
msgid "Crashed"
msgstr ""

#: downloads.c:39
msgid "Download starting…"
msgstr ""

#: badwolf.c:413
msgid "Error: Some unknown error occurred validating the certificate.\n"
msgstr ""

#: badwolf.c:394
msgid "Error: The X509 Certificate Authority is unknown.\n"
msgstr ""

#: badwolf.c:407
msgid "Error: The certificate has been revoked.\n"
msgstr ""

#: badwolf.c:404
msgid "Error: The certificate has expired. Check your system's clock.\n"
msgstr ""

#: badwolf.c:410
msgid "Error: The certificate is considered to be insecure.\n"
msgstr ""

#: badwolf.c:401
msgid "Error: The certificate isn't valid yet. Check your system's clock.\n"
msgstr ""

#: badwolf.c:397
msgid "Error: The given identity doesn't match the expected one.\n"
msgstr ""

#: keybindings.c:32
#, c-format
msgid ""
"Minimalist and privacy-oriented web browser based on WebKitGTK\n"
"Runtime WebKit version: %d.%d.%d"
msgstr ""

#: badwolf.c:904
msgid "New tab"
msgstr ""

#: badwolf.c:1102
msgid "Open new tab"
msgstr ""

#: badwolf.c:104
msgid "Out of Memory"
msgstr ""

#: badwolf.c:1032
#, c-format
msgid "Running Badwolf version: %s\n"
msgstr ""

#: badwolf.c:1039
#, c-format
msgid "Runtime WebKit version: %d.%d.%d\n"
msgstr ""

#: badwolf.c:437
#, c-format
msgid "TLS Error for %s."
msgstr ""

#: badwolf.c:440
msgid "Temporarily Add Exception"
msgstr ""

#: badwolf.c:650
msgid "Toggle javascript"
msgstr ""

#: badwolf.c:655
msgid "Toggle loading images automatically"
msgstr ""

#: badwolf.c:108
msgid "Unknown Crash"
msgstr ""

#: badwolf.c:653
msgid "_IMG"
msgstr ""

#: badwolf.c:648
msgid "_JS"
msgstr ""

#: badwolf.c:982
#, c-format
msgid "badwolf: content-filter loaded, adding to content-manager…\n"
msgstr ""

#: badwolf.c:1002 badwolf.c:1009
#, c-format
msgid "badwolf: failed to compile content-filters.json, err: [%d] %s\n"
msgstr ""

#: badwolf.c:970 badwolf.c:975
#, c-format
msgid "badwolf: failed to load content-filter, err: [%d] %s\n"
msgstr ""

#: badwolf.c:1060
#, c-format
msgid "content-filters file set to: %s\n"
msgstr ""

#: badwolf.c:792
msgid "search in current page"
msgstr ""

#: badwolf.c:99
msgid "the web process crashed.\n"
msgstr ""

#: badwolf.c:103
msgid "the web process exceeded the memory limit.\n"
msgstr ""

#: badwolf.c:107
msgid "the web process terminated for an unknown reason.\n"
msgstr ""

#: badwolf.c:1046
#, c-format
msgid "webkit-web-extension directory set to: %s\n"
msgstr ""

#. TRANSLATOR Ignore this entry. Done for forcing Unicode in xgettext.
#: badwolf.c:1138
msgid "ø"
msgstr ""
